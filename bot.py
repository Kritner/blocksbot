import asyncio
import logging
import os

import nltk

from slackclient import SlackClient
from slackclient.user import User
from slackclient.util import SearchDict, SearchList
from websocket import WebSocketConnectionClosedException

import utils
from message import Message


LOGGER = logging.getLogger(__file__)


class SlackBot(SlackClient):
    """ An async Slack Bot. """

    def __init__(self, token=os.getenv('AUTH_TOKEN'), proxies=None):
        """
        Set up the bot.

        This involves:
         - Running the training algorithm for the bots NLP classifiers.
         - Asking the Slack server for a websocker via the RTM API.
         - Collecting details on the bots own User so it can detect mentions.
         - Setting up the event loop.
         - Starting the method for handling responses.
        """

        self.connected = False

        super().__init__(token, proxies)

        self.dialoge_act_classifier = self._setup_da_classifier()
        self._train_extra()

        self._connect()

        # We require a future to be able to tell the end when to end.
        # Considering this should never stop, we don't actually tell
        # the future to end things anywhere.
        self.future = asyncio.Future()
        asyncio.ensure_future(self.handle_responses())

        # Set up the async loop.
        self.event_loop = asyncio.get_event_loop()
        self.event_loop.run_until_complete(self.future)
        self.event_loop.run_forever()

    def _connect(self):
        """
        Simple connection method to be used on initiation and
        closed connections.
        """
        if not self.rtm_connect():
            raise ConnectionError('Connection failed. Did you use the bot token?')
        self.connected = True

        self.user: User = self.users.find(self.server.username)

    @property
    def users(self) -> SearchDict:
        """
        Returns a SearchDict of users.

        You can search this dict of users like so:

            channel = self.users.find('blocksbot')

        This will return a User object which gives you access to
        properties such as:

            channel.id
            channel.tz
            channel.name
            channel.real_name
        """
        return self.server.users

    @property
    def channels(self) -> SearchList:
        """
        Returns a SearchList of channels available.

        You can search this list of channels like so:

            channel = self.channels.find('general')

        This will return a Channel object which gives you access to
        properties such as:

            channel.id
            channel.name
            channel.members
        """
        return self.server.channels

    async def handle_responses(self):
        """
        We need to eternally loop over responses over the websocket.

        Instead of holding onto resources we use async/await to allow
        context switching so the bot can listen and respond at the same time.
        """

        response_type_to_method = {
            'message': self.handle_message,
            'team_join': self.handle_new_joiner
        }

        while True:
            await asyncio.sleep(0.5)  # lets not poll too much.

            # Sometimes Slack closes the connection after an extended
            # period of time. We should try and reopen it.
            try:
                responses: list = self.rtm_read()
            except WebSocketConnectionClosedException:
                self._connect()
                responses: list = self.rtm_read()

            for response in responses:
                LOGGER.debug(str(response))
                if response.get('type') in response_type_to_method:
                    try:
                        response_type_to_method[response['type']](response)
                    except KeyError:
                        LOGGER.error(
                            'Error occurred! Got a key error with response %s',
                            str(response)
                        )

    def handle_message(self, response: dict):
        """
        Figure out what kind of act a message is.

        If it is a greeting, respond.
        """
        message = Message(response)

        if self.user.id not in message.mentioned_users:
            return

        featureset = utils.dialogue_act_features(message.text)

        act = self.dialoge_act_classifier.classify(featureset)

        if act == 'Greet':
            message.respond(self, 'Hi!')

    def handle_new_joiner(self, response: dict):
        """
        If someone new has joined we should send them a welcome message.
        """

        welcome_message = (
            "Welcome <@{user}>! We Coding Blocks folk are a quite friendly "
            "bunch. Take some time to explore the channels list, there's "
            "loads to see. Take a moment to read the code of conduct "
            "[http://www.codingblocks.net/slack-code-of-conduct/]. "
            "We try to keep it informal here (and the :codingblocks: folks "
            "have the final say on matters, obviously), but where possible we "
            "stick to the code of conduct. There's folks from all levels of "
            "development experience here, so don't be shy about shouting up "
            "with a question or sharing your point of view or experience. "
            "*Above all, have a good time and avoid <#{politics}>*.\n\n"
            "Some great channels to checkout are <#{dev_talk}>, <#{gear}>, "
            "<#{jobs}>, <#{learning_teaching}>, and <#{tips_and_tools}>. "
            "There are many more that you can join by clicking the "
            "\"Channels\" label on the upper left-hand sidebar."
        ).format(
            user=response['user']['id'],
            politics=self.channels.find('politics').id,
            dev_talk=self.channels.find('dev-talk').id,
            gear=self.channels.find('gear').id,
            jobs=self.channels.find('jobs').id,
            learning_teaching=self.channels.find('learning_teaching').id,
            tips_and_tools=self.channels.find('tips-and-tools').id
        )

        self.rtm_send_message(
            channel=self.channels.find('general').id,
            message=welcome_message
        )

    def train(self, act: str, phrase: str):
        featureset = [(utils.dialogue_act_features(phrase), act)]
        self.dialoge_act_classifier.train(featureset)

    def _setup_da_classifier(self) -> nltk.NaiveBayesClassifier:
        """
        Create a Dialogue Act classifier.

        This can analyse a message to figure out what kind of "act"
        it falls under.

        e.g. Greeting, Question etc.
        """
        nltk.download('punkt')
        nltk.download('nps_chat')
        posts = nltk.corpus.nps_chat.xml_posts()

        featuresets = [
            (
                utils.dialogue_act_features(post.text),
                post.get('class')
            )
            for post in posts
        ]

        return nltk.NaiveBayesClassifier.train(featuresets)

    def _train_extra(self):
        """
        Train a couple extra common greetings.

        Experimenting tells me this is the correct amount of repetition
        to have these phrases recognised as a greeting.

        TODO: Move these to a database and allow the bot to learn via human
        input (with all new phrases going to the DB).
        TODO: Make Async (after moving to a db). Allows us to train the bot
        passively after it is already made active with a faster warm up.
        """
        for _ in range(6):
            self.train('Greet', 'Good morning')
        for _ in range(2):
            self.train('Greet', 'Good afternoon')


if __name__ == '__main__':
    SlackBot()

import re

from slackclient import SlackClient


class Message:
    """ A wrapper around a seen message. """

    def __init__(self, content: dict):
        """ Unpack the message appropriately. """

        self.channel: str = content['channel']
        self.mentioned_users = set()

        if 'message' in content:  # This is in a thread.
            self.threaded = True
            self.user: str = content['message']['user']
            self.text: str = self._analyse_text(content['message']['text'])
            self.ts: str = content['message'].get('thread_ts')
        else:
            self.threaded = False
            self.user: str = content['user']
            self.text: str = self._analyse_text(content['text'])
            self.ts: str = content['ts']

    def respond(self, client: SlackClient, message: str):
        """ Reply to the user. """
        if not self.threaded:
            client.rtm_send_message(
                channel=self.channel,
                message=f'<@{self.user}> {message}'
            )

    def _analyse_text(self, text: str) -> str:
        """ Collect and remove all user mentions in the message. """
        a = re.finditer(r'<@(\w*)>*', text)
        self.mentioned_users = {match.groups()[0] for match in a}
        new_text = re.sub(r'\s*<@\w*>\s*', '', text)
        return new_text

    def __str__(self):
        return f'{self.text} - from user {self.user} in channel {self.channel}'
